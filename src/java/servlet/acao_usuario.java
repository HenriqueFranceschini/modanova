package servlet;

import GenericoDAO.GenericoDAO;
import entidades.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_usuario extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_usuario</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_usuario at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("inativar_usuario")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Usuario usuario = (Usuario) GenericoDAO.getObjectBanco(id, Usuario.class);
            usuario.setStatus("I");
            GenericoDAO.inativar(usuario);
            response.sendRedirect("Tela_Listagem_ADM.jsp");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String param = request.getParameter("param");

        if (param.equals("cadastrar_usuario_adm")) {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            Usuario usuario = new Usuario();
            usuario.setUsername(username);
            usuario.setPassword(password);
            usuario.setStatus("A");
            usuario.setTipo_conta("Administrativa");
            if (GenericoDAO.cadastrar(usuario)) {
                response.sendRedirect("Tela_Listagem_ADM.jsp");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void encaminharPagina(String pagina, HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Erro ao encaminhar: " + e);
        }
    }

}
