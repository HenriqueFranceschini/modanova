package servlet;

import GenericoDAO.GenericoDAO;
import apoio.Formatacao;
import entidades.Cidade;
import entidades.Cliente;
import entidades.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_cliente extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_cliente</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_cliente at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("salvar_cadastro")) {

            Cliente cliente = new Cliente();
            Cidade cidade = (Cidade) GenericoDAO.getObjectBanco(Integer.parseInt(request.getParameter("cidade")), Cidade.class);
            String cpf = Formatacao.formataCPF(request.getParameter("cpf"));
            String data = Formatacao.formataData(request.getParameter("data"));
            String nome = request.getParameter("nome");
            String endereco = request.getParameter("endereco");
            String email = request.getParameter("email");

            cliente.setName(nome);
            cliente.setCpf(cpf);
            cliente.setEmail(email);
            cliente.setStreet(endereco);
            cliente.setBirth_date(data);
            cliente.setCidade(cidade);
            cliente.setStatus("A");

            Usuario usuario = new Usuario();
            String login = request.getParameter("login");
            String senha = request.getParameter("senha");

            usuario.setUsername(login);
            usuario.setPassword(senha);
            usuario.setTipo_conta("Cliente");
            usuario.setStatus("A");

            if (GenericoDAO.cadastrar(usuario)) {
                int usuario_id = GenericoDAO.getLastId("Usuario");
                cliente.setUsuario_id(usuario_id);
                if (GenericoDAO.cadastrar(cliente)) {
                    response.sendRedirect("index.jsp");
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
