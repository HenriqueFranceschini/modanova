package servlet;

import GenericoDAO.GenericoDAO;
import entidades.Pedido;
import entidades.PedidoListagem;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_vendas extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_vendas</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_vendas at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("cancelar_pedido")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Pedido pedido = (Pedido) GenericoDAO.getObjectBanco(id, Pedido.class);
            pedido.setStatus("Cancelado");
            GenericoDAO.salvarEdicao(pedido);
            response.sendRedirect("Tela_Listagem_Vendas.jsp");
        }

        if (param.equals("finalizar_pedido")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Pedido pedido = (Pedido) GenericoDAO.getObjectBanco(id, Pedido.class);
            pedido.setStatus("Finalizado");
            GenericoDAO.salvarEdicao(pedido);
            response.sendRedirect("Tela_Listagem_Vendas.jsp");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("pesquisa")) {
            String pesquisa = request.getParameter("cliente");
            String data_inicial = request.getParameter("data_inicial");
            String data_final = request.getParameter("data_final");
            ArrayList<PedidoListagem> pl = GenericoDAO.getVendasPesquisa(pesquisa, data_inicial, data_final);
            request.setAttribute("VendasPesquisa", pl);
            encaminharPagina("Tela_Listagem_Vendas_Pesquisa.jsp", request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void encaminharPagina(String pagina, HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Erro ao encaminhar: " + e);
        }
    }

}
