package servlet;

import GenericoDAO.GenericoDAO;
import entidades.Estado;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_estado extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_estado</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_estado at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String param = request.getParameter("param");

        if (param.equals("inativar_estado")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Estado estado = (Estado) GenericoDAO.getObjectBanco(id, Estado.class);
            estado.setStatus("I");
            GenericoDAO.inativar(estado);
            response.sendRedirect("Tela_Listagem_Estado.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String param = request.getParameter("param");

        if (param.equals("salvar_cadastro_estado")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Estado estado = new Estado();
            if (id == 0) {
                estado.setName(request.getParameter("name"));
                estado.setStatus("A");
                if (GenericoDAO.cadastrar(estado)) {
                    response.sendRedirect("Tela_Listagem_Estado.jsp");
                }
            } else {
                estado.setId(id);
                estado.setName(request.getParameter("name"));
                estado.setStatus("A");
                if (GenericoDAO.salvarEdicao(estado)) {
                    response.sendRedirect("Tela_Listagem_Estado.jsp");
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void encaminharPagina(String pagina, HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Erro ao encaminhar: " + e);
        }
    }

}
