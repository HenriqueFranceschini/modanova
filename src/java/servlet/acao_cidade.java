package servlet;

import GenericoDAO.GenericoDAO;
import entidades.Cidade;
import entidades.Estado;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_cidade extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_cidade</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_cidade at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String param = request.getParameter("param");

        if (param.equals("inativar_cidade")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Cidade cidade = (Cidade) GenericoDAO.getObjectBanco(id, Cidade.class);
            cidade.setStatus("I");
            GenericoDAO.inativar(cidade);
            response.sendRedirect("Tela_Listagem_Cidade.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String param = request.getParameter("param");

        if (param.equals("salvar_cadastro_cidade")) {
            int id = Integer.parseInt(request.getParameter("id"));
            if (id == 0) {
                Cidade cidade = new Cidade();
                int idEstado = Integer.parseInt(request.getParameter("selectedstate"));
                System.out.println(idEstado);
                Estado estado = (Estado) GenericoDAO.getObjectBanco(idEstado, Estado.class);
                cidade.setName(request.getParameter("name"));
                cidade.setEstado(estado);
                cidade.setStatus("A");
                if (GenericoDAO.cadastrar(cidade)) {
                    response.sendRedirect("Tela_Listagem_Cidade.jsp");
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void encaminharPagina(String pagina, HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Erro ao encaminhar: " + e);
        }
    }

}
