package servlet;

import GenericoDAO.GenericoDAO;
import entidades.Produto;
import entidades.Tamanho;
import entidades.Tipo_Produto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_produto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_produto</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_produto at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("inativar_produto")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Produto tp = (Produto) GenericoDAO.getObjectBanco(id, Produto.class);
            tp.setStatus("I");
            GenericoDAO.inativar(tp);
            response.sendRedirect("Tela_Listagem_Produto.jsp");
        }

        if (param.equals("reativar_produto")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Produto tp = (Produto) GenericoDAO.getObjectBanco(id, Produto.class);
            tp.setStatus("A");
            GenericoDAO.salvarEdicao(tp);
            response.sendRedirect("Tela_Listagem_Produto.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");
        String idString = request.getParameter("id");

        if (param.equals("salvar_cadastro")) {
            Tamanho tamanho = (Tamanho) GenericoDAO.getObjectBanco(Integer.parseInt(request.getParameter("tamanho")), Tamanho.class);
            Tipo_Produto tp = (Tipo_Produto) GenericoDAO.getObjectBanco(Integer.parseInt(request.getParameter("tipo_produto")), Tipo_Produto.class);
            Double preco = Double.parseDouble(request.getParameter("preco"));
            int estoque = Integer.parseInt(request.getParameter("estoque"));
            String nome = request.getParameter("nome");

            if (idString.equals("0")) {
                Produto produto = new Produto();
                produto.setName(nome);
                produto.setStatus("A");
                produto.setStock(estoque);
                produto.setTamanho(tamanho);
                produto.setTipo_produto(tp);
                produto.setPrice(preco);
                if (GenericoDAO.cadastrar(produto)) {
                    response.sendRedirect("Tela_Listagem_Produto.jsp");
                }
            } else {
                Produto produto = (Produto) GenericoDAO.getObjectBanco(Integer.parseInt(idString), Produto.class);
                produto.setName(nome);
                produto.setStock(estoque);
                produto.setTamanho(tamanho);
                produto.setTipo_produto(tp);
                produto.setPrice(preco);
                if (GenericoDAO.salvarEdicao(produto)) {
                    response.sendRedirect("Tela_Listagem_Produto.jsp");
                }
            }
        }

        if (param.equals("pesquisa")) {
            String pesquisa = request.getParameter("pesquisa");
            String tp = request.getParameter("tipo_produto");
            String status = request.getParameter("status");            
            ArrayList<Produto> produtos = GenericoDAO.getPesquisaProduto(pesquisa, Integer.parseInt(tp), status);
            request.setAttribute("ProdutosPesquisa", produtos);
            encaminharPagina("Tela_Listagem_Produto_Pesquisa.jsp", request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void encaminharPagina(String pagina, HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Erro ao encaminhar: " + e);
        }
    }
}
