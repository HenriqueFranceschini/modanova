package servlet;

import GenericoDAO.GenericoDAO;
import apoio.Validacoes;
import entidades.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class acao_login extends HttpServlet {
    
    public static int id_usuario;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String param = request.getParameter("param");

        if (param.equals("logout")) {
            HttpSession sessao = request.getSession();
            sessao.invalidate();
            response.sendRedirect("index.jsp");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String param = request.getParameter("param");

        if (param.equals("logar")) {
            String usuario = request.getParameter("username");
            String senha = request.getParameter("password");
            Validacoes val = new Validacoes();
            if (val.logar(usuario, senha)) {
                ArrayList<Usuario> usu = GenericoDAO.consultarUsuarios();
                for (int i = 0; i < usu.size(); i++) {
                    Usuario u = (Usuario) usu.get(i);
                    Usuario usuarioBanco = (Usuario) GenericoDAO.getUsuario(usuario, senha);
                    if (u.getUsername().equals(usuario) && u.getPassword().equals(senha)) {
                        HttpSession sessao = ((HttpServletRequest) request).getSession();
                        sessao.setAttribute("usuario", usuarioBanco);                        
                        if (u.getTipo_conta().equals("Administrativa")) {
                            sessao.setAttribute("username", usuario);
                            encaminharPagina("Tela_Principal_ADM.jsp", request, response);
                        } else {
                            sessao.setAttribute("username", usuario);
                            id_usuario = u.getId();
                            encaminharPagina("Tela_Principal_Cliente.jsp", request, response);
                        }
                    }
                }
            } else {
                request.setAttribute("msgLogin", "erro");
                encaminharPagina("index.jsp", request, response);
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

    private void encaminharPagina(String pagina, HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestDispatcher rd = request.getRequestDispatcher(pagina);
            rd.forward(request, response);
        } catch (Exception e) {
            System.out.println("Erro ao encaminhar: " + e);
        }
    }

}
