package servlet;

import GenericoDAO.GenericoDAO;
import entidades.Cidade;
import entidades.Tipo_Produto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_tipo_produto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_tipo_produto</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_tipo_produto at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("inativar_tipo_produto")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Tipo_Produto tp = (Tipo_Produto) GenericoDAO.getObjectBanco(id, Tipo_Produto.class);
            tp.setStatus("I");
            GenericoDAO.inativar(tp);
            response.sendRedirect("Tela_Listagem_Tipo_Produto.jsp");
        }

        if (param.equals("reativar_tipo_produto")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Tipo_Produto tp = (Tipo_Produto) GenericoDAO.getObjectBanco(id, Tipo_Produto.class);
            tp.setStatus("A");
            GenericoDAO.salvarEdicao(tp);
            response.sendRedirect("Tela_Listagem_Tipo_Produto.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("salvar_cadastro")) {
            String description = request.getParameter("description");
            Tipo_Produto tp = new Tipo_Produto();
            tp.setDescription(description);
            tp.setStatus("A");
            if (GenericoDAO.cadastrar(tp)) {
                response.sendRedirect("Tela_Listagem_Tipo_Produto.jsp");
            }
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
