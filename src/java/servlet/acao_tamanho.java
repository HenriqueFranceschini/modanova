package servlet;

import GenericoDAO.GenericoDAO;
import entidades.Tamanho;
import entidades.Tipo_Produto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_tamanho extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_tamanho</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_tamanho at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("inativar_tamanho")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Tamanho tp = (Tamanho) GenericoDAO.getObjectBanco(id, Tamanho.class);
            tp.setStatus("I");
            GenericoDAO.inativar(tp);
            response.sendRedirect("Tela_Listagem_Tamanho.jsp");
        }

        if (param.equals("reativar_tamanho")) {
            int id = Integer.parseInt(request.getParameter("id"));
            Tamanho tp = (Tamanho) GenericoDAO.getObjectBanco(id, Tamanho.class);
            tp.setStatus("A");
            GenericoDAO.salvarEdicao(tp);
            response.sendRedirect("Tela_Listagem_Tamanho.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String param = request.getParameter("param");

        if (param.equals("salvar_cadastro")) {
            String number = request.getParameter("number");
            Tamanho tp = new Tamanho();
            tp.setNumber(number);
            tp.setStatus("A");
            if (GenericoDAO.cadastrar(tp)) {
                response.sendRedirect("Tela_Listagem_Tamanho.jsp");
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
