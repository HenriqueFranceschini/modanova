package servlet;

import entidades.Cliente;
import entidades.Pedido;
import entidades.Produto;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.TimeZone;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class acao_compra extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet acao_compra</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet acao_compra at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cliente_id = request.getParameter("cliente_id");
        String produto_id = request.getParameter("produto_id");
        String pagamento = request.getParameter("pagamento");
        String endereco = request.getParameter("endereco");
        String nome_cartao = request.getParameter("nome_cartao");
        String numero_cartao = request.getParameter("numero_cartao");
        int quantidade = Integer.parseInt(request.getParameter("quantidade"));

        Produto produto = (Produto) GenericoDAO.GenericoDAO.getObjectBanco(Integer.parseInt(produto_id), Produto.class);
        Pedido pedido = new Pedido();
        pedido.setAmount(1);
        pedido.setCliente((Cliente) GenericoDAO.GenericoDAO.getObjectBanco(Integer.parseInt(cliente_id), Cliente.class));
        pedido.setProduto(produto);
        pedido.setStatus("Ativo");
        pedido.setTotal(produto.getPrice());
        pedido.setForma_pagamento(pagamento);
        pedido.setStreet(endereco);
        pedido.setNome_cartao(nome_cartao);
        pedido.setNumero_cartao(numero_cartao);
        pedido.setAmount(quantidade);
        if (GenericoDAO.GenericoDAO.cadastrar(pedido)) {
            response.sendRedirect("Tela_Principal_Cliente.jsp");
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
