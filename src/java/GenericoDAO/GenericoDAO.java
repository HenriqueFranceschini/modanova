package GenericoDAO;

import apoio.ConexaoBD;
import apoio.HibernateUtil;
import entidades.Cidade;
import entidades.Cliente;
import entidades.Estado;
import entidades.PedidoListagem;
import entidades.Produto;
import entidades.Tamanho;
import entidades.Tipo_Produto;
import entidades.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import org.hibernate.*;
import java.util.ArrayList;
import java.util.List;

public class GenericoDAO {

    public static boolean cadastrar(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.save(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println("Erro ao cadastrar: " + e);
            return false;
        } finally {
            sessao.close();
        }
    }

    public static boolean inativar(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.update(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println("Erro ao inativar: " + e);
            return false;
        } finally {
            sessao.close();
        }
    }

    public static boolean delete(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.delete(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            sessao.close();
        }
    }

    public static Object getObjectBanco(int id, Class classe) {
        Session sessao = HibernateUtil.getSessionFactory().openSession();
        Transaction transacao = sessao.beginTransaction();
        Object obj = null;
        try {
            obj = sessao.get(classe, id);
            transacao.commit();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return obj;
    }

    public static Object getCliente(int usuario_id) {
        List<Object> resultado = new ArrayList();
        Object obj = null;
        Session sessao = null;
        try {
            String sql = "FROM Cliente "
                    + "WHERE usuario_id = '" + usuario_id + "'";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Cliente cliente = (Cliente) resultado.get(i);
                obj = cliente;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return obj;
    }

    public static ArrayList<Produto> getPesquisaProduto(String pesquisa, int tp, String status) {
        ArrayList<Produto> produtos = new ArrayList();
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();
            String sql = "SELECT * "
                    + "FROM produto "
                    + "WHERE name ILIKE '%" + pesquisa + "%' AND status ILIKE '%" + status + "%'";

            if (tp != 0) {
                sql += " AND tipo_produto_id = '" + tp + "'";
            }
            ResultSet resultado = st.executeQuery(sql);
            while (resultado.next()) {
                Produto produto = new Produto();
                Tamanho tamanho = (Tamanho) GenericoDAO.getObjectBanco(resultado.getInt("tamanho_id"), Tamanho.class);
                Tipo_Produto tipo_produto = (Tipo_Produto) GenericoDAO.getObjectBanco(resultado.getInt("tipo_produto_id"), Tipo_Produto.class);
                produto.setId(resultado.getInt("id"));
                produto.setName(resultado.getString("name"));
                produto.setPrice(resultado.getDouble("price"));
                produto.setStatus(resultado.getString("status"));
                produto.setStock(resultado.getInt("stock"));
                produto.setTamanho(tamanho);
                produto.setTipo_produto(tipo_produto);
                produtos.add(produto);
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
        }
        return produtos;
    }

    public static String getClienteLogado(String usuario) {
        String cli = "";
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();
            String sql = "SELECT c.name "
                    + "FROM cliente c, usuario u "
                    + "WHERE c.usuario_id = u.id AND u.username = '" + usuario + "'";

            ResultSet resultado = st.executeQuery(sql);
            while (resultado.next()) {
                cli = resultado.getString("name");
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
        }
        return cli;
    }

    public static ArrayList<PedidoListagem> getVendas() {
        ArrayList<PedidoListagem> pedidos = new ArrayList();
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();
            String sql = "SELECT p.id, c.name AS Cliente, pro.name AS Produto, t.number, cid.name AS Cidade, c.street, p.status "
                    + "FROM pedido p, cliente c, produto pro, tamanho t, cidade cid "
                    + "WHERE p.produto_id = pro.id AND pro.tamanho_id = t.id AND c.cidade_id = cid.id AND p.cliente_id = c.id";
            ResultSet resultado = st.executeQuery(sql);
            while (resultado.next()) {
                PedidoListagem pl = new PedidoListagem();
                pl.setId(resultado.getInt("id"));
                pl.setCliente(resultado.getString("Cliente"));
                pl.setProduto(resultado.getString("Produto"));
                pl.setTamanho(resultado.getString("number"));
                pl.setCidade(resultado.getString("Cidade"));
                pl.setEndereco(resultado.getString("street"));
                pl.setStatus(resultado.getString("status"));
                pedidos.add(pl);
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
        }
        return pedidos;
    }

    public static ArrayList<PedidoListagem> getVendasCliente(String usuario) {
        ArrayList<PedidoListagem> pedidos = new ArrayList();
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();
            String sql = "SELECT p.id, c.name AS Cliente, pro.name AS Produto, t.number, cid.name AS Cidade, c.street, p.status "
                    + "FROM pedido p, cliente c, produto pro, tamanho t, cidade cid, usuario u "
                    + "WHERE p.produto_id = pro.id AND pro.tamanho_id = t.id AND c.cidade_id = cid.id AND p.cliente_id = c.id AND c.usuario_id = u.id AND u.username ILIKE '%" + usuario + "%'";
            ResultSet resultado = st.executeQuery(sql);
            while (resultado.next()) {
                PedidoListagem pl = new PedidoListagem();
                pl.setId(resultado.getInt("id"));
                pl.setCliente(resultado.getString("Cliente"));
                pl.setProduto(resultado.getString("Produto"));
                pl.setTamanho(resultado.getString("number"));
                pl.setCidade(resultado.getString("Cidade"));
                pl.setEndereco(resultado.getString("street"));
                pl.setStatus(resultado.getString("status"));
                pedidos.add(pl);
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
        }
        return pedidos;
    }

    public static ArrayList<PedidoListagem> getVendasPesquisa(String cliente, String data_inicial, String data_final) {
        ArrayList<PedidoListagem> pedidos = new ArrayList();
        try {
            Statement st = ConexaoBD.getInstance().getConnection().createStatement();
            String sql = "SELECT p.id, c.name AS Cliente, pro.name AS Produto, t.number, cid.name AS Cidade, c.street, p.status "
                    + "FROM pedido p, cliente c, produto pro, tamanho t, cidade cid "
                    + "WHERE p.produto_id = pro.id AND pro.tamanho_id = t.id AND c.cidade_id = cid.id AND p.cliente_id = c.id AND c.name ILIKE '%" + cliente + "%' ";

            if (!data_inicial.isEmpty()) {
                data_inicial = data_inicial.replaceAll("\\D", "-");
                char[] di = data_inicial.toCharArray();
                String data_ini = String.valueOf(di[6]) + String.valueOf(di[7]) + String.valueOf(di[8]) + String.valueOf(di[9]) + String.valueOf(di[5]) + String.valueOf(di[3]) + String.valueOf(di[4]) + String.valueOf(di[2]) + String.valueOf(di[0]) + String.valueOf(di[1]);
                sql += "AND p.date BETWEEN '" + data_ini + "' ";
            } else {
                sql += "AND p.date BETWEEN '2021-01-01' ";
            }

            if (!data_final.isEmpty()) {
                data_final = data_final.replaceAll("\\D", "-");
                char[] df = data_final.toCharArray();
                String data_fin = String.valueOf(df[6]) + String.valueOf(df[7]) + String.valueOf(df[8]) + String.valueOf(df[9]) + String.valueOf(df[5]) + String.valueOf(df[3]) + String.valueOf(df[4]) + String.valueOf(df[2]) + String.valueOf(df[0]) + String.valueOf(df[1]);
                sql += "AND '" + data_fin + "'";
            } else {
                sql += "AND '2030-01-01'";
            }

            ResultSet resultado = st.executeQuery(sql);
            while (resultado.next()) {
                PedidoListagem pl = new PedidoListagem();
                pl.setId(resultado.getInt("id"));
                pl.setCliente(resultado.getString("Cliente"));
                pl.setProduto(resultado.getString("Produto"));
                pl.setTamanho(resultado.getString("number"));
                pl.setCidade(resultado.getString("Cidade"));
                pl.setEndereco(resultado.getString("street"));
                pl.setStatus(resultado.getString("status"));
                pedidos.add(pl);
            }
        } catch (Exception e) {
            System.out.println("ERRO: " + e);
        }
        return pedidos;
    }

    public static String getCountMes(String mes) {
        String count = "";
        Session sessao = HibernateUtil.getSessionFactory().openSession();
        Transaction transacao = sessao.beginTransaction();
        try {
            String sql = "";
            switch (mes) {
                case "01":
                case "03":
                case "05":
                case "07":
                case "08":
                case "10":
                case "12":
                    sql = "FROM Pedido "
                            + "WHERE date BETWEEN '2021-" + mes + "-01' AND '2021-" + mes + "-31'";
                    break;
                case "02":
                    sql = "FROM Pedido "
                            + "WHERE date BETWEEN '2021-02-01' AND '2021-02-28'";
                    break;
                case "04":
                case "06":
                case "09":
                case "11":
                    sql = "FROM Pedido "
                            + "WHERE date BETWEEN '2021-" + mes + "-01' AND '2021-" + mes + "-30'";
                    break;
                default:
                    break;
            }
            org.hibernate.Query query = sessao.createQuery(sql);
            count = String.valueOf(sessao.createQuery(sql).list().size());
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return count;
    }

    public static Object getUsuario(String username, String password) {
        List<Object> resultado = new ArrayList();
        Object obj = null;
        Session sessao = null;
        try {
            String sql = "FROM Usuario "
                    + "WHERE username = '" + username + "' AND password = '" + password + "'";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Usuario usuario = (Usuario) resultado.get(i);
                obj = usuario;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return obj;
    }

    public static ArrayList<Usuario> consultarUsuarios() {
        List<Object> resultado = new ArrayList();
        ArrayList<Usuario> usu = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Usuario "
                    + "ORDER BY username";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Usuario usuario = (Usuario) resultado.get(i);
                usu.add(usuario);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return usu;
    }

    public static ArrayList<Estado> consultarEstados() {
        List<Object> resultado = new ArrayList();
        ArrayList<Estado> est = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Estado "
                    + "ORDER BY name";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Estado estado = (Estado) resultado.get(i);
                est.add(estado);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return est;
    }

    public static ArrayList<Cidade> consultarCidades() {
        List<Object> resultado = new ArrayList();
        ArrayList<Cidade> cid = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Cidade "
                    + "ORDER BY name";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Cidade cidade = (Cidade) resultado.get(i);
                cid.add(cidade);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return cid;
    }

    public static ArrayList<Tipo_Produto> consultarTipoProdutos() {
        List<Object> resultado = new ArrayList();
        ArrayList<Tipo_Produto> tp = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Tipo_Produto "
                    + "ORDER BY description";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Tipo_Produto tipo_prod = (Tipo_Produto) resultado.get(i);
                tp.add(tipo_prod);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return tp;
    }

    public static ArrayList<Tamanho> consultarTamanhos() {
        List<Object> resultado = new ArrayList();
        ArrayList<Tamanho> tamanho = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Tamanho "
                    + "ORDER BY number";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Tamanho tam = (Tamanho) resultado.get(i);
                tamanho.add(tam);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return tamanho;
    }

    public static ArrayList<Produto> consultarProdutos() {
        List<Object> resultado = new ArrayList();
        ArrayList<Produto> produto = new ArrayList();
        Session sessao = null;
        try {
            String sql = "FROM Produto "
                    + "ORDER BY name";
            sessao = HibernateUtil.getSessionFactory().openSession();
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Produto prod = (Produto) resultado.get(i);
                produto.add(prod);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return produto;
    }

    public static boolean salvarEdicao(Object obj) {
        Session sessao = null;
        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction transacao = sessao.beginTransaction();
            sessao.update(obj);
            transacao.commit();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            sessao.close();
        }
    }

    public static int getLastId(String classe) {
        Session sessao = null;
        int maiorId = 0;
        try {
            String sql = "SELECT MAX(id) FROM " + classe;
            sessao = HibernateUtil.getSessionFactory().openSession();
            maiorId = Integer.parseInt(sessao.createQuery(sql).list().toString().replaceAll("\\D", ""));
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return maiorId;
    }

    public byte[] gerarRelatorioProdutos() {
        try {
            Connection conn = ConexaoBD.getInstance().getConnection();
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/relatorios/Listagem_Produtos.jrxml"));
            Map parameters = new HashMap();
            byte[] bytes = JasperRunManager.runReportToPdf(relatorio, parameters, conn);
            return bytes;
        } catch (Exception e) {
            System.out.println("erro ao gerar relatorio: " + e);
        }
        return null;
    }

    public byte[] gerarRelatorioVenda(int id) {
        try {
            Connection conn = ConexaoBD.getInstance().getConnection();
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/relatorios/Relatorio_Venda.jrxml"));
            Map parameters = new HashMap();
            parameters.put("idPedido", id);
            byte[] bytes = JasperRunManager.runReportToPdf(relatorio, parameters, conn);
            return bytes;
        } catch (Exception e) {
            System.out.println("erro ao gerar relatorio: " + e);
        }
        return null;
    }

    public byte[] gerarRelatorioCidades() {
        try {
            Connection conn = ConexaoBD.getInstance().getConnection();
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/relatorios/Listagem_Cidades.jrxml"));
            Map parameters = new HashMap();
            byte[] bytes = JasperRunManager.runReportToPdf(relatorio, parameters, conn);
            return bytes;
        } catch (Exception e) {
            System.out.println("erro ao gerar relatorio: " + e);
        }
        return null;
    }

    public byte[] gerarRelatorioClientes() {
        try {
            Connection conn = ConexaoBD.getInstance().getConnection();
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/relatorios/Listagem_Clientes.jrxml"));
            Map parameters = new HashMap();
            byte[] bytes = JasperRunManager.runReportToPdf(relatorio, parameters, conn);
            return bytes;
        } catch (Exception e) {
            System.out.println("erro ao gerar relatorio: " + e);
        }
        return null;
    }

    public byte[] gerarRelatorioUsuarios() {
        try {
            Connection conn = ConexaoBD.getInstance().getConnection();
            JasperReport relatorio = JasperCompileManager.compileReport(getClass().getResourceAsStream("/relatorios/Listagem_Usuarios.jrxml"));
            Map parameters = new HashMap();
            byte[] bytes = JasperRunManager.runReportToPdf(relatorio, parameters, conn);
            return bytes;
        } catch (Exception e) {
            System.out.println("erro ao gerar relatorio: " + e);
        }
        return null;
    }
}
