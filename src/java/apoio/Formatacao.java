package apoio;

public class Formatacao {

    public static String formataData(String data) {
        String dataFormatada = data.replace("/", "-");
        return dataFormatada;
    }
    
    public static String formataCPF(String cpf) {
        String cpfFormatado = cpf.replace(".", "");
        String cpfFormatado2 = cpfFormatado.replace("-", "");
        return cpfFormatado2;
    }
}
