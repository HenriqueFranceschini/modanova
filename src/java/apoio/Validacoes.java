package apoio;

import entidades.Usuario;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;

public class Validacoes {

    public boolean logar(String login, String senha) {
        boolean pas = false;
        Session sessao = HibernateUtil.getSessionFactory().openSession();
        List<Usuario> resultado = new ArrayList();
        String sql = "FROM Usuario";
        try {
            org.hibernate.Query query = sessao.createQuery(sql);
            resultado = query.list();
            for (int i = 0; i < resultado.size(); i++) {
                Usuario usuario = resultado.get(i);
                if (login.equals(usuario.getUsername()) && senha.equals(usuario.getPassword())) {
                    pas = true;
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            sessao.close();
        }
        return pas;
    }
}
