<%-- 
    Document   : Tela_Listagem_Produto_Pesquisa
    Created on : 7 de jun de 2021, 16:13:47
    Author     : henri
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="GenericoDAO.GenericoDAO"%>
<%@page import="entidades.Tipo_Produto"%>
<%@page import="entidades.Tamanho"%>
<%@page import="entidades.Produto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
    </head>

    <body>
        <header class="p-3 bg-dark">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="Tela_Principal_ADM.jsp" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                    <svg class="bi me-2" width="40" height="32"></svg>
                    <span class="fs-4">Dashboard</span>
                </a>
                <h4 class="text-center text-white">Produtos Cadastrados</h4>
                <ul class="nav me-lg-auto"></ul>

                <div class="dropdown text-end ">
                    <a href="#" class="d-block link-secondary text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="/NovaModa/img/foto_usuario.png" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="/NovaModa/acao_login?param=logout">Sair</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="d-flex">
            <div class="d-flex flex-column p-3 text-white bg-dark" style="width: 280px; height: 900px;">
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">

                    <li class="nav-item">
                        <a href="Tela_Listagem_ADM.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Administradores
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Cidade.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Cidades
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Estado.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Estados
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Tipo_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tipos de Produtos
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Tamanho.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tamanhos
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Produtos
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Vendas.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Vendas
                        </a>
                    </li>
                </ul>
            </div>

            <div class="container">
                <div class="row">

                    <form name="formproduto" method="post" action="/NovaModa/acao_produto?param=pesquisa">
                        <br>
                        <div class="d-flex">
                            <div class="col-md-3">
                                <label for="state" class="form-label">Tipos de Produtos</label>
                                <select class="form-select" name="tipo_produto" >
                                    <option value="0">Escolha... </option>
                                    <%
                                        ArrayList<Tipo_Produto> tipo_produto_lista = new GenericoDAO().consultarTipoProdutos();
                                        for (int i = 0; i < tipo_produto_lista.size(); i++) {
                                            Tipo_Produto tipo_produto = tipo_produto_lista.get(i);
                                            if (tipo_produto.getStatus().equals("A")) {
                                    %>
                                    <option value="<%=tipo_produto.getId()%>"> <%=tipo_produto.getDescription()%> </option>
                                    <%
                                            }
                                        }
                                    %>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label for="state" class="form-label">Status</label>
                                <select class="form-select" name="status" >
                                    <option value="A"> Ativo </option>
                                    <option value="I"> Inativo </option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="state" class="form-label">Descrição do Produto</label>
                                <input type="text" class="form-control" name="pesquisa" placeholder="Pesquisa...">
                            </div>
                            <button type="submit" class="btn btn-dark active" role="button">Pesquisar</button>
                        </div>
                    </form>

                    <main class="col-sm-12 ml-sm-auto col-md-12 " role="main">
                        <div>
                            <section layout:fragment="content">
                                <div>
                                    <br>

                                    <div class="text-end">
                                        <a href="/NovaModa/Tela_Cadastro_Produto.jsp?id=0" class="btn btn-dark active" role="button" aria-pressed="true"><i class="fas fa-plus"></i>Adicionar</a>
                                        <a href="List_Prod.jsp" class="btn btn-dark active" role="button" aria-pressed="true">Listagem</a>
                                    </div>

                                    <div class="row">
                                        <br>
                                    </div>
                                    <div class="row">
                                        <table class="table table-striped ">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Nome</th>
                                                    <th scope="col">Tipo</th>
                                                    <th scope="col">Tamanho</th>
                                                    <th scope="col">Estoque</th>
                                                    <th scope="col">Preço</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Opções</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <%
                                                    ArrayList<Produto> produtos = (ArrayList) request.getAttribute("ProdutosPesquisa");
                                                    if (produtos != null) {
                                                        for (int i = 0; i < produtos.size(); i++) {
                                                            Produto produto = (Produto) produtos.get(i);
                                                            Tamanho tamanho = produto.getTamanho();
                                                            Tipo_Produto tp = produto.getTipo_produto();
                                                %>
                                                <tr>
                                                    <td><%= produto.getId()%></td>  
                                                    <td><%= produto.getName()%></td>
                                                    <td><%= tp.getDescription()%></td>  
                                                    <td><%= tamanho.getNumber()%></td>  
                                                    <td><%= produto.getStock()%></td>  
                                                    <td><%= produto.getPrice()%></td>  
                                                    <td><%= produto.getStatus()%></td>  
                                                    <td> 
                                                        <a href="/NovaModa/Tela_Cadastro_Produto.jsp?id=<%= produto.getId()%>" class="btn btn-success" title="Editar">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <a href="/NovaModa/acao_produto?param=reativar_produto&id=<%= produto.getId()%>" class="btn btn-success" title="Reativar">
                                                            <i class="fas fa-plus"></i>
                                                        </a>
                                                        <a href="/NovaModa/acao_produto?param=inativar_produto&id=<%= produto.getId()%>" class="btn btn-danger" title="Inativar">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </body>
</html>
