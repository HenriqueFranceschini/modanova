<%-- 
    Document   : Tela_Pedido_Detalhado
    Created on : 21 de jun de 2021, 21:06:53
    Author     : henri
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>
        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    </head>

    <body>
        <header class="p-3 bg-dark">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="Tela_Principal_ADM.jsp" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                    <svg class="bi me-2" width="40" height="32"></svg>
                    <span class="fs-4">Dashboard</span>
                </a>
                <h4 class="text-center text-white">Vendas</h4>
                <ul class="nav me-lg-auto"></ul>

                <div class="dropdown text-end ">
                    <a href="#" class="d-block link-secondary text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="/NovaModa/img/foto_usuario.png" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="/NovaModa/acao_login?param=logout">Sair</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="d-flex">
            <div class="d-flex flex-column p-3 text-white bg-dark" style="width: 280px; height: 900px;">
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="Tela_Listagem_ADM.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Administradores
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Tela_Listagem_Cidade.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Cidades
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Tela_Listagem_Estado.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Estados
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Tela_Listagem_Tipo_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tipos de Produtos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Tela_Listagem_Tamanho.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tamanhos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Tela_Listagem_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Produtos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="Tela_Listagem_Vendas.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Vendas
                        </a>
                    </li>
                </ul>
            </div>

            <center>
                <div class="row">
                    <br>
                </div>

                <div class="form-group col-md-3">
                    <label for="Nome">*Nome Completo</label>
                    <input type="text" class="form-control" name="nome" placeholder="Digite aqui..." required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*CPF</label>
                    <input type="text" class="form-control" name="cpf" placeholder="Digite aqui..." data-mask="000.000.000-00" required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Digite aqui..." required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Data de Nascimento</label>
                    <input type="text" class="form-control" name="data" placeholder="Digite aqui..." data-mask="00/00/0000" required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Endereço Completo</label>
                    <input type="text" class="form-control" name="endereco" placeholder="Digite aqui..." required="">
                </div>
                <br>

                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Login de Acesso</label>
                    <input type="text" class="form-control" name="login" placeholder="Digite aqui..." required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Senha de Acesso</label>
                    <input type="password" class="form-control" name="senha" placeholder="Digite aqui..." required="">
                </div>
                <br>
            </center>

    </body>
</html>
