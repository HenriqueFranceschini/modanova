<%@page import="java.util.ArrayList"%>
<%@page import="entidades.Cidade"%>
<%@page import="GenericoDAO.GenericoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/tela_login.css" rel="stylesheet">    

        <script language="JavaScript" src="js/validacoesCadastroCliente.js"></script>

    </head>

    <body class="text-center">
        <header class="p-3 bg-dark text-white ">
            <div class="container">
                <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">                       
                    <ul class="me-lg-auto mb-2 mb-md-0"><h3>Nova Moda</h3></ul>
                    <div class="text-end">
                        <a type="button" href="/NovaModa/index.jsp" class="btn btn-outline-light me-2">Entrar</a>
                        <a type="button" href="/NovaModa/Tela_Cadastro_Cliente.jsp" class="btn btn-primary me-2">Cadastre-se</a>
                    </div>
                </div>
            </div>
        </header>

        <form name='formcliente' method='post' action="/NovaModa/acao_cliente?param=salvar_cadastro" onSubmit="return validarCadastro();">
            <center>
                <div class="row">
                    <br>
                </div>

                <div class="form-group col-md-3">
                    <label for="Nome">*Nome Completo</label>
                    <input type="text" class="form-control" name="nome" placeholder="Digite aqui..." required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*CPF</label>
                    <input type="text" class="form-control" name="cpf" placeholder="Digite aqui..." data-mask="000.000.000-00" required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Email</label>
                    <input type="text" class="form-control" name="email" placeholder="Digite aqui..." required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Data de Nascimento</label>
                    <input type="text" class="form-control" name="data" placeholder="Digite aqui..." data-mask="00/00/0000" required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Endereço Completo</label>
                    <input type="text" class="form-control" name="endereco" placeholder="Digite aqui..." required="">
                </div>
                <br>
                <div class="col-md-4">
                    <label for="state" class="form-label">*Cidade</label>
                    <select class="form-select" name="cidade" style="width: 250px;">
                        <option>Escolha...</option>
                        <%
                            ArrayList<Cidade> cid = new GenericoDAO().consultarCidades();
                            for (int i = 0; i < cid.size(); i++) {
                                Cidade cidade = cid.get(i);
                                if (cidade.getStatus().equals("A")) {
                        %>
                        <option value="<%=cidade.getId()%>"> <%=cidade.getName()%> </option>
                        <%
                                }
                            }
                        %>
                    </select>
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Login de Acesso</label>
                    <input type="text" class="form-control" name="login" placeholder="Digite aqui..." required="">
                </div>
                <br>
                <div class="form-group col-md-3">
                    <label for="Nome">*Senha de Acesso</label>
                    <input type="password" class="form-control" name="senha" placeholder="Digite aqui..." required="">
                </div>
                <br>
            </center>

            <button class="btn btn-dark" type="submit">Cadastrar</button>

        </form>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

    </body>
</html>
