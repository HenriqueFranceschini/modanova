<%-- 
    Document   : Tela_Cadastro_Cidade
    Created on : 18 de abr de 2021, 19:03:03
    Author     : henri
--%>

<%@page import="GenericoDAO.GenericoDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entidades.Estado"%>
<%@page import="entidades.Cidade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <script language="JavaScript" src="js/salvar_edicao_estado.js"></script>
        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    </head>

    <body>
        <header class="p-3 mb-3 border-bottom bg-dark">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                <ul class="me-lg-auto"></ul>

                <h4 class="text-center text-white">Cadastro de Cidade</h4>

                <ul class="nav me-lg-auto"></ul>

                <div class="text-end">
                    <a href="/NovaModa/Tela_Listagem_Cidade.jsp" class="btn btn-outline-light me-2" role="button" aria-pressed="true">Voltar</a>
                </div>
            </div>
        </header>

        <%
            Cidade cidade = new Cidade();
            if (request.getParameter("option").equals("cadastrar")) {
                cidade.setId(0);
                cidade.setName("");
            } else {
                int id = Integer.parseInt(String.valueOf(request.getParameter("id")));
                cidade.setId(id);
                cidade.setName(String.valueOf(request.getParameter("name")));
            }
        %>

        <div id="CadastroUsuario">
            <form name='formcidade' id="FormPessoa" method='post' action="/NovaModa/acao_cidade?param=salvar_cadastro_cidade&id=<%=cidade.getId()%>">
                <center>

                    <div class="row">
                        <br>
                        <br>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="Nome">*Nome da Cidade</label>
                        <input type="text" class="form-control" id="Nome" name="name" aria-describedby="Nome" placeholder="Digite aqui..." required="" value="<%= cidade.getName()%>">
                    </div>

                    <div class="col-md-4">
                        <label for="state" class="form-label">*Estado</label>
                        <select class="form-select" id="state" name="selectedstate" style="width: 250px;">
                            <option value="0">Escolha...</option>
                            <%
                                ArrayList<Estado> estado = new GenericoDAO().consultarEstados();
                                for (int i = 0; i < estado.size(); i++) {
                                    Estado est = estado.get(i);
                                    if (est.getStatus().equals("A")) {
                            %>
                            <option value="<%=est.getId()%>"> <%=est.getName()%> </option>
                            <%
                                    }
                                }
                            %>
                        </select>
                    </div>

                    <br>

                    <button type="submit" class="btn btn-dark">Salvar</button>

                </center>
            </form>
        </div>
    </body>
</html>
