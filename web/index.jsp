<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/tela_login.css" rel="stylesheet">    

        <script language="JavaScript" src="js/validacoesLogin.js"></script>
    </head>

    <body class="text-center">
        <main>        
            <header class="p-3 bg-dark text-white ">
                <div class="container">
                    <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">                       
                        <ul class="me-lg-auto mb-2 mb-md-0"><h3>Nova Moda</h3></ul>
                        <div class="text-end">
                            <a type="button" href="/NovaModa/index.jsp" class="btn btn-outline-light me-2">Entrar</a>
                            <a type="button" href="/NovaModa/Tela_Cadastro_Cliente.jsp" class="btn btn-primary me-2">Cadastre-se</a>
                        </div>
                    </div>
                </div>
            </header>

            <main class="form-signin">
                <form name="formusuario" method="post" action="/NovaModa/acao_login?param=logar" onSubmit="return validarDadosLogin();">
                    <h1 class="h2 mb-3 fw-normal">Acesse sua Conta</h1>
                    <div class="form-floating">
                        <input type="text" name="username" class="form-control" id="floatingInput" placeholder="Username">
                        <label for="floatingInput">Username</label>
                    </div>
                    <div class="form-floating">
                        <input type="password" name="password" class="form-control" id="floatingPassword" placeholder="Password">
                        <label for="floatingPassword">Password</label>
                    </div>
                    <%
                        String msg = String.valueOf(request.getAttribute("msgLogin"));
                        if (msg.equals("erro")) {
                    %>
                    <p id = "msgErroLogin">Usuário ou senha não conferem!</p>                
                    <%
                        }
                    %>
                    <button class="w-100 btn btn-lg btn-primary" type="submit">Entrar</button>           
                </form>
            </main>
        </main>
    </body>
</html>
