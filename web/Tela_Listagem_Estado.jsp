<%@page import="GenericoDAO.GenericoDAO"%>
<%@page import="entidades.Estado"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
    </head>

    <body>
        <header class="p-3 bg-dark">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="Tela_Principal_ADM.jsp" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                    <svg class="bi me-2" width="40" height="32"></svg>
                    <span class="fs-4">Dashboard</span>
                </a>
                <h4 class="text-center text-white">Estados</h4>
                <ul class="nav me-lg-auto"></ul>

                <div class="dropdown text-end ">
                    <a href="#" class="d-block link-secondary text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="/NovaModa/img/foto_usuario.png" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="/NovaModa/acao_login?param=logout">Sair</a></li>
                    </ul>
                </div>
            </div>
        </header>

        <div class="d-flex">
            <div class="d-flex flex-column p-3 text-white bg-dark" style="width: 280px; height: 900px;">
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">

                    <li class="nav-item">
                        <a href="Tela_Listagem_ADM.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Administradores
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Cidade.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                            Cidades
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Estado.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                            Estados
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Tipo_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tipos de Produtos
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Tamanho.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tamanhos
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Produtos
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="Tela_Listagem_Vendas.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Vendas
                        </a>
                    </li>
                </ul>
            </div>

            <div class="container">
                <div class="row">
                    <main class="col-sm-12 ml-sm-auto col-md-12 " role="main">
                        <div>
                            <section layout:fragment="content">
                                <div class="container">
                                    <br>
                                    <div class="text-end">
                                        <a href="/NovaModa/Tela_Cadastro_Estado.jsp?option=cadastrar" class="btn btn-dark active" role="button" aria-pressed="true"><i class="fas fa-plus"></i>Adicionar</a>
                                    </div>

                                    <div class="row">
                                        <br>
                                    </div>

                                    <div class="row">
                                        <table class="table table-striped ">
                                            <thead>
                                                <tr>
                                                    <th scope="col">ID</th>
                                                    <th scope="col">Estado</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Opções</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                    ArrayList<Estado> estado = new GenericoDAO().consultarEstados();
                                                    for (int i = 0; i < estado.size(); i++) {
                                                        Estado est = estado.get(i);
                                                        if (est.getStatus().equals("A")) {
                                                %>
                                                <tr>
                                                    <td><%= est.getId()%></td>  
                                                    <td><%= est.getName()%></td>  
                                                    <td><%= est.getStatus()%></td>  
                                                    <td>
                                                        <a href="/NovaModa/Tela_Cadastro_Estado.jsp?option=editar&id=<%= est.getId()%>&name=<%= est.getName()%>" class="btn btn-success" title="Editar">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                        <a href="/NovaModa/acao_estado?param=inativar_estado&id=<%= est.getId()%>" class="btn btn-danger" title="Excluir">
                                                            <i class="fas fa-trash"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </body>
</html>
