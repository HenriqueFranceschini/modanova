<%@page import="entidades.Usuario"%>
<%@page import="servlet.acao_login"%>
<%@page import="entidades.PedidoListagem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entidades.Produto"%>
<%@page import="GenericoDAO.GenericoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>        
        
        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark" aria-label="Third navbar example">
            <div class="container ">
                <a class="navbar-brand" href="Tela_Principal_Cliente.jsp"> <h3>Nova Moda</h3> </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExample03">
                    <ul class="navbar-nav me-auto mb-2 mb-sm-0">
                        <li><a class="btn btn-sm btn-outline-secondary" href="/NovaModa/acao_vendas?param=visualisa_pedido_cliente">Compras Realizadas</a></li>
                    </ul>
                </div>
                <div class="dropdown text-end ">
                    <a href="#" class="d-block link-secondary text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="/NovaModa/img/foto_usuario.png" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="/NovaModa/acao_login?param=logout">Sair</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="" role="main">
            <div class="container">
                <section layout:fragment="content">
                    <div class="row">
                        <br>
                    </div>
                    <div class="row">
                        <table class="table table-striped ">
                            <thead>
                                <tr>
                                    <th scope="col">Produto</th>
                                    <th scope="col">Tamanho</th>
                                    <th scope="col">Endereço</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Opções</th>
                                </tr>
                            </thead>

                            <tbody>
                                <%
                                    int usuario_id = acao_login.id_usuario;
                                    Usuario usuario = (Usuario) GenericoDAO.getObjectBanco(usuario_id, Usuario.class);
                                    System.out.println(usuario_id);
                                    ArrayList<PedidoListagem> pedido_lista = GenericoDAO.getVendasCliente(usuario.getUsername());
                                    for (int i = 0; i < pedido_lista.size(); i++) {
                                        PedidoListagem pedido = (PedidoListagem) pedido_lista.get(i);
                                %>
                                <tr>
                                    <td><%= pedido.getProduto()%></td>
                                    <td><%= pedido.getTamanho()%></td>
                                    <td><%= pedido.getCidade() + ", " + pedido.getEndereco()%></td>
                                    <td><%= pedido.getStatus()%></td>
                                    <td> 
                                        <a href="/NovaModa/acao_vendas?param=cancelar_pedido&id=<%=pedido.getId()%>" class="btn btn-dark" title="Cancelar">
                                            <i class="fas fa-times-circle"></i>
                                        </a>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        </main>
    </body>
</html>
