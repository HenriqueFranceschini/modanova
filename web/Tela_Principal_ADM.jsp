<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">

        <link href="css/navbar_adm.css" rel="stylesheet">    

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {'packages': ['bar']});
            google.charts.setOnLoadCallback(drawStuff);
            <%
                String janeiro = GenericoDAO.GenericoDAO.getCountMes("01");
                String fevereiro = GenericoDAO.GenericoDAO.getCountMes("02");
                String marco = GenericoDAO.GenericoDAO.getCountMes("03");
                String abril = GenericoDAO.GenericoDAO.getCountMes("04");
                String maio = GenericoDAO.GenericoDAO.getCountMes("05");
                String junho = GenericoDAO.GenericoDAO.getCountMes("06");
                String julho = GenericoDAO.GenericoDAO.getCountMes("07");
                String agosto = GenericoDAO.GenericoDAO.getCountMes("08");
                String setembro = GenericoDAO.GenericoDAO.getCountMes("09");
                String outubro = GenericoDAO.GenericoDAO.getCountMes("10");
                String novembro = GenericoDAO.GenericoDAO.getCountMes("11");
                String dezembro = GenericoDAO.GenericoDAO.getCountMes("12");
            %>
            function drawStuff() {
                var data = new google.visualization.arrayToDataTable([
                    ['Mês', 'Vendas'],
                    ["Janeiro", <%=janeiro%>],
                    ["Fevereiro", <%=fevereiro%>],
                    ["Março", <%=marco%>],
                    ["Abril", <%=abril%>],
                    ['Maio', <%=maio%>],
                    ['Junho', <%=junho%>],
                    ['Julho', <%=julho%>],
                    ['Agosto', <%=agosto%>],
                    ['Setembro', <%=setembro%>],
                    ['Outubro', <%=outubro%>],
                    ['Novembro', <%=novembro%>],
                    ['Dezembro', <%=dezembro%>]
                ]);

                var options = {
                    width: 1500,
                    legend: {position: 'none'},
                    axes: {
                        x: {
                            0: {side: 'top', label: 'Vendas Mensais'}
                        }
                    },
                    bar: {groupWidth: "90%"}
                };

                var chart = new google.charts.Bar(document.getElementById('top_x_div'));
                chart.draw(data, google.charts.Bar.convertOptions(options));
            }
            ;
        </script>
        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>

    </head>

    <body>
        <header class="p-3 bg-dark">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                <a href="Tela_Principal_ADM.jsp" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                    <svg class="bi me-2" width="40" height="32"></svg>
                    <span class="fs-4">Dashboard</span>
                </a>
                <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0 "></ul>

                <div class="dropdown text-end ">
                    <a href="#" class="d-block link-secondary text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="/NovaModa/img/foto_usuario.png" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="/NovaModa/acao_login?param=logout">Sair</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="d-flex">
            <div class="d-flex flex-column p-3 text-white bg-dark" style="width: 280px; height: 900px;">
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">

                    <li class="nav-item">
                        <a href="Tela_Listagem_ADM.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Administradores
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Cidade.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                            Cidades
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Estado.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                            Estados
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Tipo_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tipos de Produtos
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Tamanho.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Tamanhos
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="Tela_Listagem_Produto.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Produtos
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a href="Tela_Listagem_Vendas.jsp" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16"></svg>
                            Vendas
                        </a>
                    </li>
                </ul>
            </div>

            <div id="top_x_div" style="width: 1400px; height: 800px;"></div>
        </div>       
    </body>
</html>
