<%@page import="entidades.Tamanho"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Moda Nova</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
    </head>
    
    <body>
        <header class="p-3 mb-3 border-bottom bg-dark">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                <ul class="me-lg-auto"></ul>

                <h4 class="text-center text-white">Cadastro de Tamanhos</h4>

                <ul class="nav me-lg-auto"></ul>

                <div class="text-end">
                    <a href="/NovaModa/Tela_Listagem_Tamanho.jsp" class="btn btn-outline-light me-2" role="button" aria-pressed="true">Voltar</a>
                </div>
            </div>
        </header>

        <%
            Tamanho tp = new Tamanho();
        %>

        <div id="CadastroEstado">
            <form name='FormTam' id="FormTP" method='post' action="/NovaModa/acao_tamanho?param=salvar_cadastro">
                <center>

                    <div class="row">
                        <br>
                        <br>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="Nome">*Número</label>
                        <input type="text" class="form-control" id="Nome" name="number" aria-describedby="Nome" placeholder="Digite aqui..." required="">
                    </div>

                    <br>

                    <button type="submit" class="btn btn-dark">Salvar</button>

                </center>
            </form>
        </div>
    </body>
</html>
