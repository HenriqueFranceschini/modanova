<%@page import="java.util.ArrayList"%>
<%@page import="entidades.Produto"%>
<%@page import="GenericoDAO.GenericoDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_cli.css" rel="stylesheet">    
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="apple-touch-icon" href="/docs/5.0/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
        <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
        <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
        <link rel="manifest" href="/docs/5.0/assets/img/favicons/manifest.json">
        <link rel="mask-icon" href="/docs/5.0/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
        <link rel="icon" href="/docs/5.0/assets/img/favicons/favicon.ico">
        <meta name="theme-color" content="#7952b3">

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
    </head>

    <body>
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark" aria-label="Third navbar example">
            <div class="container ">
                <a class="navbar-brand" href="Tela_Principal_Cliente.jsp"> <h3>Nova Moda</h3> </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExample03">
                    <ul class="navbar-nav me-auto mb-2 mb-sm-0">
                        <li><a class="btn btn-sm btn-outline-secondary" href="/NovaModa/Tela_Visualisa_Pedido.jsp">Compras Realizadas</a></li>
                    </ul>

                </div>
                <div class="dropdown text-end ">
                    <a href="#" class="d-block link-secondary text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="/NovaModa/img/foto_usuario.png" width="32" height="32" class="rounded-circle">
                    </a>
                    <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
                        <li><hr class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="/NovaModa/acao_login?param=logout">Sair</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="album py-5 bg-light">
            <div class="container">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    <%
                        ArrayList<Produto> prod = new GenericoDAO().consultarProdutos();
                        for (int i = 0; i < prod.size(); i++) {
                            Produto produto = prod.get(i);

                    %>
                    <div class="col">
                        <div class="card shadow-sm">
                            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>

                            <div class="card-body">
                                <p class="card-text"><%= produto.getName()%></p>
                                <div class="d-flex justify-content-between align-items-center">                                    
                                    <small class="text-muted">R$ <%= produto.getPrice()%></small>
                                    <div class="btn-group">
                                        <a type="button" class="btn btn-sm btn-outline-secondary" href="/NovaModa/Tela_Compra.jsp?id=<%=produto.getId()%>">Comprar</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <%
                        }
                    %>
                </div>
            </div>
        </div>

    </body>
</html>
