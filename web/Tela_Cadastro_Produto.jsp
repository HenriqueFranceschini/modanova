<%@page import="entidades.Tamanho"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entidades.Tipo_Produto"%>
<%@page import="GenericoDAO.GenericoDAO"%>
<%@page import="entidades.Produto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nova Moda</title>

        <link href=" css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar_adm.css" rel="stylesheet">    
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">

        <script src="js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
    </head>

    <body>
        <header class="p-3 mb-3 border-bottom bg-dark">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">

                <ul class="me-lg-auto"></ul>

                <h4 class="text-center text-white">Cadastro de Produto</h4>

                <ul class="nav me-lg-auto"></ul>

                <div class="text-end">
                    <a href="/NovaModa/Tela_Listagem_Produto.jsp" class="btn btn-outline-light me-2" role="button" aria-pressed="true">Voltar</a>
                </div>
            </div>
        </header>

        <%
            Produto produto = new Produto();
            int id = Integer.parseInt(request.getParameter("id"));
            if (id != 0) {
                produto = (Produto) GenericoDAO.getObjectBanco(id, Produto.class);
            } else {
                produto.setId(0);
                produto.setName("");
                produto.setPrice(000.00);
            }
        %>

        <div id="CadastroEstado">
            <form name='FormCadProd' id="FormPessoa" method='post' action="/NovaModa/acao_produto?param=salvar_cadastro&id=<%=produto.getId()%>">
                <center>

                    <div class="row">
                        <br>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="Nome">*Descrição</label>
                        <input type="text" class="form-control" id="Nome" name="nome" aria-describedby="Nome" placeholder="Digite aqui..." required="" value="<%= produto.getName()%>">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="Nome">*Estoque</label>
                        <input type="text" class="form-control" id="Nome" name="estoque" aria-describedby="Nome" placeholder="Digite aqui..." required="" value="<%= produto.getStock()%>" data-mask="###">
                    </div>

                    <div class="form-group col-md-3">
                        <label for="Nome">*Preço</label>
                        <input type="text" class="form-control" id="Nome" name="preco" aria-describedby="Nome" placeholder="Digite aqui..." required="" value="<%= produto.getPrice()%>">
                    </div>

                    <%
                        if (id != 0) {
                            Tamanho tamanho = produto.getTamanho();
                            Tipo_Produto tipo_produto = produto.getTipo_produto();
                    %>
                    <div class="col-md-4">
                        <label for="state" class="form-label">*Tamanho</label>
                        <select class="form-select" id="state" name="tamanho" style="width: 250px;">
                            <option value="<%=tamanho.getId()%>"> <%=tamanho.getNumber()%></option>
                            <%
                                ArrayList<Tamanho> tamanhoLista = new GenericoDAO().consultarTamanhos();
                                for (int i = 0; i < tamanhoLista.size(); i++) {
                                    Tamanho tam = tamanhoLista.get(i);
                                    if (tam.getStatus().equals("A") && tam.getId() != tamanho.getId()) {
                            %>
                            <option value="<%=tam.getId()%>"> <%=tam.getNumber()%> </option>
                            <%
                                    }
                                }
                            %>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label for="state" class="form-label">*Tipo da Roupa</label>
                        <select class="form-select" id="state" name="tipo_produto" style="width: 250px;">
                            <option value="<%= tipo_produto.getId()%>"> <%= tipo_produto.getDescription()%> </option>
                            <%
                                ArrayList<Tipo_Produto> tp = new GenericoDAO().consultarTipoProdutos();
                                for (int i = 0; i < tp.size(); i++) {
                                    Tipo_Produto tipo_produto2 = tp.get(i);
                                    if (tipo_produto2.getStatus().equals("A") && tipo_produto2.getId() != tipo_produto.getId()) {
                            %>
                            <option value="<%=tipo_produto2.getId()%>"> <%=tipo_produto2.getDescription()%> </option>
                            <%
                                    }
                                }
                            %>
                        </select>
                    </div>

                    <%
                    } else {
                    %>

                    <div class="col-md-4">
                        <label for="state" class="form-label">*Tamanho</label>
                        <select class="form-select" id="state" name="tamanho" style="width: 250px;">
                            <option value="0">Escolha...</option>
                            <%
                                ArrayList<Tamanho> tamanho = new GenericoDAO().consultarTamanhos();
                                for (int i = 0; i < tamanho.size(); i++) {
                                    Tamanho tam = tamanho.get(i);
                                    if (tam.getStatus().equals("A")) {
                            %>
                            <option value="<%=tam.getId()%>"> <%=tam.getNumber()%> </option>
                            <%
                                    }
                                }
                            %>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label for="state" class="form-label">*Tipo da Roupa</label>
                        <select class="form-select" id="state" name="tipo_produto" style="width: 250px;">
                            <option value="0">Escolha...</option>
                            <%
                                ArrayList<Tipo_Produto> tp = new GenericoDAO().consultarTipoProdutos();
                                for (int i = 0; i < tp.size(); i++) {
                                    Tipo_Produto tipo_produto = tp.get(i);
                                    if (tipo_produto.getStatus().equals("A")) {
                            %>
                            <option value="<%=tipo_produto.getId()%>"> <%=tipo_produto.getDescription()%> </option>
                            <%
                                    }
                                }
                            %>
                        </select>
                    </div>

                    <%
                        }
                    %>

                    <br>

                    <button type="submit" class="btn btn-dark">Salvar</button>

                </center>
            </form>
        </div>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>"
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
        
    </body>
</html>
