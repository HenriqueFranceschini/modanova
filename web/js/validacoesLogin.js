function validarDadosLogin() {

    var erro = 0;
    username = document.formusuario.username.value;
    password = document.formusuario.password.value;

    if (username.length < 3 || password.length < 3) {
        erro++;
    }

    if (erro > 0) {
        window.alert("Preencha os campos corretamente!");
        return false;
    } else {
        return true;
    }
}

