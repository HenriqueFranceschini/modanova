function validarCadastro() {

    var erro = 0;
    name = document.formcliente.nome.value;
    cpf = document.formcliente.cpf.value;
    email = document.formcliente.email.value;
    data = document.formcliente.data.value;
    endereco = document.formcliente.endereco.value;
    username = document.formcliente.login.value;
    password = document.formcliente.senha.value;

    if (username.length < 3 || password.length < 3 || cpf.length < 14 || email.length < 18 || data.length < 10) {
        erro++;
    }

    if (erro > 0) {
        window.alert("Preencha os campos corretamente!");
        return false;
    } else {
        return true;
    }
}
